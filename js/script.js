// $(function() {
  // jQuery goes here...

  // Uncomment this line to fade out the red box on page load
  
  // $(".red-box").fadeOut(2000).fadeIn(1000);
  // $(".green-box").fadeOut(2000);
  // $(".red-box").fadeTo(1000, 0.5);

  // $('.red-box').fadeTo(1000, 0.2);
  // $('.green-box').fadeTo(2000, 0.4);
  // $('.blue-box').fadeTo(3000, 0.8);

  // $('.green-box').fadeOut();
  // $('.green-box').fadeIn().fadeTo(1000, 0.5);

  // $('.blue-box').hide(1000);
  // $('.blue-box').show(1000);
  // $('.blue-box').toggle(1000);

  // $('.blue-box').slideToggle(2000);

  // $('p').hide();
  // $('p').slideDown(2000);

 //  $('.blue-box').animate({
 //  	"margin-left": "+=200px"
 //  }, 1000, "linear");

 //  $('.blue-box').animate({
 //  	"margin-left": "0px"
 //  }, 1000, "swing");

 //  $(".blue-box").click(function(){
	// 	$(".blue-box").slideUp(1000).slideDown(900);
	// });	


// $('.blue-box').animate({
// 	"margin-left" : "200px",
// 	"opacity": "0",
// 	"width": "50px",
// 	"height": "50px",
// 	"margin-top": "25px"
// }, 1000);

// $('p').hover(function(){
// $('p').animate({
// 	"font-size": "20px"
// }, 1000);
// });

// 	$(".red-box").fadeTo(1000, 0.2);
// 	$(".green-box").delay(1000).fadeTo(1000, 0.5);
// 	$(".blue-box").delay(2000).fadeTo(1000, 0.8).fadeOut().delay(2000).fadeIn().animate({
// 		"margin-left": "200px"
// 	}, 2000);

// });


// $(function(){
// 	$(".red-box").fadeTo(2000, 0.1, function(){
// 		alert (" Hey! It's test for Callback function! yay!");
// 	});
// });

// $(function(){
// 	$(".lightbox").delay(500).fadeIn(1000);
// });


// $(function(){
	// $("p").css("background-color", "rgba(255,0,0,0.5)");
	// $(".red-box").css("background-color", "rgba(255,0,0,0.5)");
	// $("#list").css("background-color", "rgba(255,0,0,0.5)");
	// $("input[type='email']").css("background-color", "rgba(255,0,0,0.5)");
	// $("h2, p, input").css("background-color", "rgba(255,0,0,0.5)");
	// $("li:last").css("background-color", "rgba(255,0,0,0.5)");
	// $("li:odd").css("background-color", "rgba(255,0,0,0.5)");
	// $("input:text").css("background-color", "rgba(255,0,0,0.5)");
// 	$("li:nth-child(2)").css("background-color", "rgba(255,0,0,0.5)");
// });


// jQuery Traversing

// $(function (){
	// $("#list").find("li").css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").children("li").css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").parent().css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").parents("body").css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").siblings(":header").css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").prev().css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").next().css("background-color", "rgba(0,0,255,0.5)");
	// $("#list").siblings(":header").next().css("background-color", "rgba(0,0,255,0.5)");
// 	$("form").find("input[type='password']").css("background-color", "rgba(0,0,255,0.5)");
// });


// jQuery filtering

// $(function(){
	// $("#list").find("li").filter(":even").css("border", "1px solid #f00");
	// $("#list").children("li").filter(":odd").css("border", "1px solid #f00");
	
	// $("li").filter(function(index){
	// 	return index%3 === 1;
	// }).css("border", "1px solid #f00");
	
	// $("li").first().css("border", "1px solid #f00");
	// $("li").last().css("border", "1px solid #f00");

	// $("li").eq(4).css("border", "1px solid #f00");
	// $("li").eq(-2).css("border", "1px solid #f00");

// 	$("li").not(":even").css("border", "1px solid #f00");
// });

// Adding new elements to the DOM
$(function (){
	// $("ul ul:first").append("<li> I'm gonna be the last sub-item </li>");

	// $("<li>I'm gonna be the last item.</li>").appendTo($("ul ul:nth(1)"));

	// $("ul ul").prepend("<li> I'm gonna be the new item.</li>");

	$("<li>I'm gonna be the sub item.</li>").prependTo("ul ul:nth(2)");

	$("<h4>Anilcha Maharzan</h4>").prependTo("#content");

	// $(".red-box").after("<div class='red-box'>Hello red</div>");
	// $(".blue-box").before("<div class='blue-box'>Another blue box</div>");

	// $(".green-box").before(function(){
	// 	return "<div class='green-box'>Green 2 box </div>";
	// });

	// $(".red-box").before($(".blue-box"));

	// $("p").after($("#list"));

	$(".red-box").after("<div class='red-box'>Another red box</div>");
	$(".green-box").after("<div class='green-box'>Another green box</div>");
	$(".blue-box").after("<div class='blue-box'>Another blue box</div>");

});
